<?php

declare(strict_types=1);
namespace Drupal\tour_de_fields\Hooks;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\hux\Attribute\Alter;
use Drupal\tour\Entity\Tour;
use Drupal\tour\TourInterface;

final class DynamicToursAttacher {


  use StringTranslationTrait;

  public function __construct(
    private readonly AccountProxyInterface $currentUser,
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  #[Alter('form')] // hook_form_alter
  public function hookFormAlter(&$form, FormStateInterface $formState, $form_id): void {
    /** @see \tour_page_bottom */
    if (!$this->currentUser->hasPermission('access tour')) {
      return;
    }

    $tours = [];
    $formObject = $formState->getFormObject();
    if (
      $formObject instanceof EntityFormInterface
      && ($entity = $formObject->getEntity())
      && $entity instanceof ContentEntityInterface
      && ($fieldSettings = $this->getFieldSettings($entity->getEntityTypeId(), $entity->bundle()))
    ) {
      $tours[] = $this->getTour($fieldSettings);
      foreach ($fieldSettings as $fieldSetting) {
        if (isset($form[$fieldSetting->fieldName])) {
          $form[$fieldSetting->fieldName] = [
            'tour_de_fields_icon' => [
              '#type' => 'component',
              '#component' => 'tour_de_fields:info-icon',
              '#props' => [
                'id' => $fieldSetting->getTipId(),
              ],
            ],
          ] + $form[$fieldSetting->fieldName];
        }
      }
    }

    if (!empty($tours)) {
      /** @see \Drupal\tour\TourViewBuilder::viewMultiple */
      $form['tour_de_fields'] = $this->entityTypeManager
        ->getViewBuilder('tour')
        ->viewMultiple($tours);
    }
  }

  /**
   * @param list<\Drupal\tour_de_fields\Models\TourDeFieldsFieldSettings> $fieldSettings
   */
  private function getTour(array $fieldSettings): ?TourInterface {
    $tour = NULL;

    $tips = [];
    foreach ($fieldSettings as $fieldSetting) {
      /** @see \Drupal\tour\Plugin\tour\tip\TipPluginText */
      /** @link https://www.drupal.org/docs/8/api/tour-api/overview */
      $fiendNameKebabCase = strtr($fieldSetting->fieldName, ['_' => '-']);
      $tips[$fieldSetting->getTipId()] = [
        'id' => $fieldSetting->getTipId(),
        'plugin' => 'text',
        'label' => $fieldSetting->fieldLabel,
        'body' => $fieldSetting->formHelpText,
        'weight' => 0, // @todo Retrieve weight from somewhere.
        'selector' => ".field--name-$fiendNameKebabCase",
      ];
    }

    // @todo Filter out field tips not on the form.
    // @todo Add weight matching the form.
    /** @see \Drupal\Core\Entity\HtmlEntityFormController::getFormObject */
    /** @see \Drupal\Core\Entity\EntityTypeManager::getFormObject */
    /** @see \Drupal\Core\Entity\EntityType::getFormClass */
    /** @see \Drupal\Core\Entity\Entity\EntityFormDisplay::collectRenderDisplay */

    if ($tips) {
      $tour = Tour::create([
        'module' => 'tour_de_fields',
        'label' => strval($this->t('Field help tour')),
        'tips' => $tips,
        'routes' => [], // Not needed for dynamic tour.
      ]);
    }
    return $tour;
  }

  /**
   * @return list<\Drupal\tour_de_fields\Models\TourDeFieldsFieldSettings>
   */
  private function getFieldSettings(string $entityTypeId, string $bundle): array {
    $settings = [];
    foreach (FieldConfig::loadMultiple() as $fieldConfig) {
      assert($fieldConfig instanceof FieldConfigInterface);
      // @todo Find a way to do this for base fields too.
      // @todo Move this condition to the query.
      if ($fieldConfig->getTargetEntityTypeId() === $entityTypeId && $fieldConfig->getTargetBundle() === $bundle) {
        $maybeSettings = FieldFormTourHooks::getTourDeFieldsFieldSettings($fieldConfig);
        if ($maybeSettings) {
          $settings[] = $maybeSettings;
        }
      }
    }
    return $settings;
  }

}
