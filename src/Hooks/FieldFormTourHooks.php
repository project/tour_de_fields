<?php

declare(strict_types=1);
namespace Drupal\tour_de_fields\Hooks;

use Drupal\Component\Render\HtmlEscapedText;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\FieldConfigInterface;
use Drupal\hux\Attribute\Alter;
use Drupal\tour_de_fields\Models\TourDeFieldsFieldSettings;

/**
 * @internal Can change any time.
 */
final class FieldFormTourHooks {

  use StringTranslationTrait;

  #[Alter('form_field_config_edit_form')] # hook_form_FORM_ID_alter
  public function alterFieldEditForm(&$form, FormStateInterface $form_state, $form_id): void {
    /** @see \Drupal\field_ui\Form\FieldConfigEditForm::form */
    $form['third_party_settings']['tour_de_fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tour de fields'),
    ];
    $subForm =& $form['third_party_settings']['tour_de_fields'];

    /** @var \Drupal\Core\Entity\EntityFormInterface $formObject */
    $formObject = $form_state->getFormObject();
    $fieldConfig = $formObject->getEntity();
    assert($fieldConfig instanceof FieldConfigInterface);

    $subForm['form_help'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Form help text'),
      '#default_value' => $fieldConfig->getThirdPartySetting('tour_de_fields', 'form_help'),
      '#rows' => 5,
      '#description' => $this->t('Help text that is accessed via a toggle tip, and can be navigated previous / next.'),
    ];
  }

  public static function getTourDeFieldsFieldSettings(FieldConfigInterface $fieldConfig): ?TourDeFieldsFieldSettings {
    $fieldLabel = $fieldConfig->label();
    if (!$fieldLabel) {
      $fieldLabel = t('Field @field', ['@field' => $fieldConfig->getName()]);
    }
    if (!$fieldLabel instanceof MarkupInterface) {
      $fieldLabel = new HtmlEscapedText($fieldLabel);
    }
    $help = $fieldConfig->getThirdPartySetting('tour_de_fields', 'form_help');

    $hasSettings = !empty($help);

    return $hasSettings ? new TourDeFieldsFieldSettings(
      $fieldConfig->getTargetEntityTypeId(),
      $fieldConfig->getTargetBundle(),
      $fieldConfig->getName(),
      $fieldLabel,
      $help,
    ) : NULL;
  }

}
