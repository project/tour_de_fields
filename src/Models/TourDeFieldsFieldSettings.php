<?php

declare(strict_types=1);
namespace Drupal\tour_de_fields\Models;

use Drupal\Component\Render\MarkupInterface;

/**
 * Tour-de-Fields field settings value.
 *
 * @internal
 */
final class TourDeFieldsFieldSettings {

  public function __construct(
    public readonly string $entityTypeId,
    public readonly string $bundle,
    public readonly string $fieldName,
    public readonly MarkupInterface $fieldLabel,
    public readonly string $formHelpText,
  ) {}

  public function getTipId(): string {
    $id = "tour-de-fields__field__{$this->entityTypeId}__{$this->bundle}__{$this->fieldName}";
    // tour transforms this, so do it too.
    return strtr($id, ['_' => '-']);
  }

}
