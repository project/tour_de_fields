((Drupal) => {
  Drupal.behaviors.tour_de_fields = {
    attach(context) {
      once('tour_de_fields', '.tour-de-fields-icon', context).forEach(
        function(element) {
          element.addEventListener('click', (e) => {
            let id = e.target.dataset.tourDeFieldsId;
            Drupal.tour.setActive();
            Drupal.tour.getPromise().then(function(tour) {
              tour.show(id);
            });
          });
        }
      );
    },
  };

})(Drupal)

