# Tour De Fields

## How to use
- Install the module
- Apply the patch from https://www.drupal.org/project/tour/issues/3458331 (auto-done if you have composer-patches enabled and configured to allow package-provided patches)
- On any field config, find "Tour de fields" text input, add some, e.g. for the "News" node type
- For any such node type, go to a create or edit form
- Find a ⓘ icon beneath each tagged field widget, that will start the tour at the right position
